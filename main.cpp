/*
 * main.cpp
 *
 *  Created on: Feb 2, 2019
 *  Author: Catherine DeJager
 *  For: CS 232, Project 01 at Calvin College
 *
 *  Project 1: System Calls and Utilities
 */
#include <cstdlib>  // atoi
#include <iostream>
#include <string>  // library for string::at
#include "NanoTimer.h"
//#include <sstream>  // library for stringstream
#include <unistd.h>  // library for fork, getopt
using namespace std;

static void show_usage(string name) {
	std::cerr << "Usage: " << name << " <option(s)>\n"
	              << "Options:\n"
	              << "\t-rn: compute the average time of n repetitions of the system call. If this switch is omitted, then a default of 1 should be used for n.\n"
	              << "\t-p or -process: time process creation (the fork() system call).\n"
				  << "\t-t or -thread: time thread creation (the pthread_create() system call).\n"
	              << std::endl;
}

/*
 * time a process, using fork()
 * TODO: should I pass the timer in, or is that not necessary?
 * currently only happens 1 time
 * FIXME: cout time as 0. Actual value is nonzero, the issue is with display.
 * FIXME: everything after the fork is run twice
 */
double timeProcess(NanoTimer timer, int reps) {
	int pid;
	//int total_time;
	timer.reset();
	timer.start();
	/*
	* Man page of fork() http://man7.org/linux/man-pages/man3/fork.3p.html
	* "On success, the PID of the child process is returned in the parent, and 0 is returned in the child.
	* On failure, -1 is returned in the  parent, no child process is created, and errno is set appropriately."
	*/
	pid = fork();  // return 0 to the child process and the process id of the child process to the parent process
	if (pid == 0) {
		timer.stop();  // stop the timer only when we've gotten to the child process, because NanoTimer.stop() changes the value of the timer
		cout << "Hello from the child process (PID = " << getpid() << ")" << endl;
		//cout << "Time of process creation (timeProcess): " << timer.getTotalTime() << endl;
		return timer.getTotalTime();
	}
//	cout << "Return value of fork = " << pid << endl;
//	cout << "Current process id of process = " << getpid() << endl;
//	cout << "Parent process id of current process = " << getppid() << endl;
//	if (timer.getTotalTime() == 0) {
//		cout << "Total time was 0" << endl;
//	}
	return -1.0;
}

/*
 * All code you write should be carefully documented.
 * Each file should have a "header" comment that explains
 * who wrote it, when, where, why, and the purpose of this file's code within the context of the project.
 * You should also use "inline" comments to explain any "tricky" parts in your code.
 * You should use descriptive identifiers for class, variable, and method names, should use white space effectively and consistently,
 * and anything else needed to make your code as readable as possible (by someone other than yourself).
 */

/*
 * Command line switches:
 * -rn compute the average time of n repetitions of the system call. If this switch is omitted, then a default of 1 should be used for n.
 * -p time process creation (the fork() system call).
 * -process same as -p.
 * -t time thread creation (the pthread_create() system call).
 * -thread same as -t.
 * If none of -process, -p, -thread, nor -t are specified, your program should display a "Usage" error message and terminate.
 *
 * To illustrate:

  callTimer -p
  callTimer -process
should each display the time required to create a process, while
  callTimer -t
  callTimer -thread
should each display the time required to create a thread, and
  callTimer -r100 -p -t
  callTimer -process -r100 -thread
  callTimer -p -thread -r100
  callTimer -r100 -t -process
should display the times to create processes and threads, averaged over 100 repetitions.
You will need to use the main function parameters argc and argv to process these command-line switches.
The user should be able to specify the switches in any order.
 * Problem: I could use getopt and it would make my life simpler, but getopt expects options one character long
 * and getopt_long expects that options longer than one character start with --
 * See https://linux.die.net/man/3/getopt for details
 * parent -> child. want the child to end?
 * How do I kill my child?
 */
int main(int argc, char* argv[]) {
	int reps = 1;  // number of repetitions of the system call
	bool process = false;
	bool thread = false;
	NanoTimer myTimer;

	myTimer.start();
	for (int i = 1; i < argc; i++) {
		string arg = string(argv[i]);
		if (arg == "-p" or arg == "-process") {
			process = true;
		}
		else if (arg == "-t" or arg == "-thread") {
			thread = true;
		}
		else if (arg.at(0) == '-' and arg.at(1) == 'r') {
			// get the rest of the argument and convert it to an integer. Problem: atoi has undefined behavior if given something that is not an integer
			reps = atoi(arg.substr(2).c_str());
			if (reps < 1) { // invalid number of reps
				show_usage(argv[0]);
				return 1;
			}
		}
		if (!process and !thread) {
			show_usage(argv[0]);
			return 1;
		}
	}
	cout << endl;
	cout << "process: " << process << endl;
	cout << "thread: " << thread << endl;
	cout << "reps: " << reps << endl;
	myTimer.stop();
	cout << "Time to read arguments: " << myTimer.getTotalTime() << endl;
	myTimer.stop();
	cout << "Time after stopping a stopped timer: " << myTimer.getTotalTime() << endl;
	if (process) {
		double process_time = timeProcess(myTimer, reps);
		if (process_time != -1.0) {
			cout << "Time of process creation: " << process_time << endl;
			//exit(2);
		}
		else {
			//exit(3);
			cout << "parent" << endl;
		}
		cout << "bye" << endl;
//		process_time = timeProcess(myTimer, reps);
//		if (process_time != -1.0) {
//			cout << "Time of process creation: " << process_time << endl;
//		}
//		else {
//			cout << "parent2" << endl;
//		}
//		cout << "bye" << endl;
	}
	return 0;
}
